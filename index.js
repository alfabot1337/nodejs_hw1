const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {
	const LOGS_PATH = './logs.json';
	const FILES_PATH = './file';

	const fileNotExist = path => !fs.existsSync(path);

	const createFilesDir = () => {
		if (fileNotExist(FILES_PATH)) {
			fs.mkdirSync(FILES_PATH);
		}
	};

	const createHtmlResponse = (res, code, content) => {
		res.writeHead(code, { 'Content-type': 'text/html' });
		res.end(content);
	};

	const createLogs = () => {
		if (fileNotExist(LOGS_PATH)) {
			fs.writeFile(LOGS_PATH, '{}', err => {
				if (err) {
					throw err;
				}
			});
		}
	};

	const writeLogs = (newData = {}) => {
		fs.readFile(LOGS_PATH, 'utf8', (err, data) => {
			if (err) {
				createLogs();
			}

			data = data || '{}';
			const { logs = [] } = JSON.parse(data);
			logs.push(newData);

			fs.writeFile(LOGS_PATH, JSON.stringify({ logs }), err => {
				if (err) {
					throw err;
				}
			});
		});
	};

	const filterLogs = async (from = 0, to = Date.now()) => {
		let { logs } = await readLogs();
		logs = logs.filter(({ time }) => time > from && time < to);
		return { logs };
	};

	const readLogs = () => {
		return new Promise(resolve => {
			fs.readFile(LOGS_PATH, 'utf8', (err, data) => {
				if (err) {
					createLogs();
				}
				const logs = data ? JSON.parse(data).logs || [] : [];
				resolve({ logs });
			});
		});
	};

	const server = http.createServer((req, res) => {
		const {
			pathname,
			query: { filename, content, from, to },
		} = url.parse(req.url, true);

		if (req.method === 'POST') {
			if (filename && content && pathname === '/file') {
				const filePath = `.${pathname}/${filename}`;

				fs.writeFile(filePath, content, err => {
					if (err) {
						const errMessage = `Something went wrong when writing file`;
						return createHtmlResponse(res, 500, errMessage);
					}
				});
				createHtmlResponse(res, 200, 'Your file has been added');
				writeLogs({
					message: `New file with name '${filename}' saved`,
					time: Date.now(),
				});
			} else {
				const errMessage = `Bad write file request`;
				return createHtmlResponse(res, 400, errMessage);
			}
		} else if (req.method === 'GET') {
			if (pathname === '/logs') {
				filterLogs(from, to).then(filteredLogs => {
					createHtmlResponse(res, 200, JSON.stringify(filteredLogs));
					writeLogs({
						message: `File with name '${pathname}' been read`,
						time: Date.now(),
					});
				});
			} else {
				fs.readFile(`.${pathname}`, (err, data) => {
					if (err) {
						const errMessage = `Could not find file with name ${pathname}`;
						return createHtmlResponse(res, 400, errMessage);
					}
					createHtmlResponse(res, 200, data);
					writeLogs({
						message: `File with name '${pathname}' been read`,
						time: Date.now(),
					});
				});
			}
		}
	});

	createFilesDir();
	createLogs();
	server.listen(process.env.PORT || 8080);
};